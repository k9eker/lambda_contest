package com.kspt.group1.lambdaserver;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;

/**
 *
 * @author Garik
 */
public class LambdaServerApplication extends Application {
    // URI of the root directory.  

    public static final String ROOT_URI = "file:///E:/page/";
    public static final String name_page = "index.htm";

    @Override
    public void start() throws Exception {
        // Create the HTTP server and listen on port 8182
        Component component = new Component();
        component.getServers().add(Protocol.HTTP, 8182);
        component.getClients().add(Protocol.FILE);

        // Then attach it to the local host 
        component.getDefaultHost().attach("/maps", MapResource.class);
        component.getDefaultHost().attach("/solve", ResolveResource.class);
        // Create an application  
        Application application = new Application() {
            @Override
            public Restlet createInboundRoot() {
                Directory d = new Directory(getContext(), ROOT_URI);
                d.setIndexName(name_page);
                return d;
            }
        };
        component.getDefaultHost().attach(application);
        
        // Now, let's start the component!  
        // Note that the HTTP server connector is also automatically started.  
        component.start();
    }
}
