package com.kspt.group1.lambdaserver.logic;

import com.kspt.group1.lambdaserver.simulator.Cell;
import com.kspt.group1.lambdaserver.simulator.Map;

/**
 * Класс-карта для реализации алгоритма А*.
 * Наследует реализацию обычной карты класса simulator.Map
 * @author Илья
 */
public class AstarMap extends Map {
    public static final int delta_x[] = {0, 0, -1, 0, 0, 0, 1, 0, 0};
    public static final int delta_y[] = {0, 0, 0, 0, 1, 0, 0, 0, -1};
    public static final String moves[] = {" ", " ", "R", " ", "U", " ", "L", " ", "D"}; 
   
    private int w, h;
    private AstarCell cells[][];
    private AstarCell start, goal;
    
    public AstarMap(String mapString) {
        super(mapString);
        
        Map map = new Map(mapString);
        this.w = map.getMapSize().getX();
        this.h = map.getMapSize().getY();
        
        cells = new AstarCell[w][h];
        int cost = 1;

        for (int y = 0 ; y < h ; y++) {
            for (int x = 0 ; x < w ; x++) {
                if (map.getCell(x + 1, y + 1) == Cell.WALL) cost = 0x7F;
                cells[x][y] = new AstarCell(x, y, cost);
                cost = 1;
            }
        }
        start = cells[map.getRobotPosition().getX() - 1][map.getRobotPosition().getY() - 1];
        
        goal = cells[map.getLiftPosition().getX() - 1][map.getLiftPosition().getY() - 1];
   }
    
    public AstarCell getGoal() {
        return goal;
    }

    public AstarCell getStart() {
        return start;
    }

    public AstarCell getAstarCell(int x, int y) {
        return cells[x][y];
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setGoal(AstarCell Astar_cell) {
        goal = Astar_cell;
    }

    public void setStart(AstarCell Astar_cell) {
        start = Astar_cell;
    }

    public void setGoal(int x, int y) {
        setGoal(cells[x][y]);
    }

    public void setStart(int x, int y) {
        setStart(cells[x][y]);
    }
    
    public void CleanPath() {
        for(int x = 0 ; x < w ; x++) {
            for(int y = 0 ; y < h ; y++) {
                cells[x][y].clearPathFlag();
            }
        }
    }
    public AstarMap clone(String map) {
        AstarMap astar_map = new AstarMap(map);

        astar_map.w = w;
        astar_map.h = h;

        astar_map.cells = new AstarCell[h][w];
        for(int x = 0 ; x < w ; x++) {
            for(int y = 0 ; y < h ; y++) {
                astar_map.cells[x][y] = cells[x][y].clone();
            }
        }

        astar_map.start = astar_map.cells[start.x][start.y];
        astar_map.goal = astar_map.cells[goal.x][goal.y];

        return astar_map;
    }

    public String toString() {
        String s = new String("W: " + w + " H: " + h + "\n");

        for(int x = 0 ; x < w ; x++) {
            for(int y = 0 ; y < h ; y++) {
                if(cells[x][y] == goal){
                    s += "G";
                }else if(cells[x][y] == start){
                    s += "S";
                }else if(cells[x][y].isBlocked()){
                    s += "X";
                }else if(cells[x][y].isPathFlagOn()){
                    s += ".";
                }else{
                    s += " ";
                }
            }
            s += "\n";
        }
        return s;
   }
}
