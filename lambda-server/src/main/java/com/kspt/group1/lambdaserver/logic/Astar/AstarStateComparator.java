package com.kspt.group1.lambdaserver.logic.Astar;

import java.util.Comparator;

/**
 * Комаратор над состояниями карты.
 * Используется для PriorityQueue
 * @author Колтон Семен 5081/14
 */
public class AstarStateComparator implements Comparator<AstarState> {
    
    public AstarStateComparator() {
    }

    /**
     * Сравнение двух состояний в зависимости от оценок этих состояний
     * @param o1 первое состояние
     * @param o2 второе состояние
     * @return отрицательное, нулевое или положительное число в зависимости от
     * того, первое меньше, равно или больше второго состояния
     */
    @Override
    public int compare(AstarState o1, AstarState o2) {
        if (o1.getF() < o2.getF())
            return -1;
        else if (o1.getF() == o2.getF())
            return 0;
        else
            return 1;
    }
    
}
