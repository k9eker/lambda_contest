package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.logic.Logic;
import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Pair;
import com.kspt.group1.lambdaserver.simulator.Simulator;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Логика ищущая путь до ближайшей лябды взамен оптимального пути
 * @author Колтон Семен 5081/14
 */
public class ClosestLambdaLogic implements Logic{

    // симулятор
    private Simulator sim;
    // текущее решение
    private String currentSolve;
    // текущее состояние игры
    private GameState currentState;
        
    public ClosestLambdaLogic(String map) {
        this.sim = new Simulator(map);
        this.currentState = sim.getGameState();
        this.currentSolve = "";
    }
    
    
    @Override
    public String solve() {
        Pair closestLambda;
        Heuristic he;
        Problem pr;
        AstarLogic al;
        String pathToClosestLambda;
        ArrayList<Pair> lambdaList = this.currentState.getMap().getLambdaList();
        // собираем все лямбды
        while(!lambdaList.isEmpty()){
            closestLambda = defineClosestLambda(lambdaList, this.currentState.getMap().getRobotPosition());
            he = new ManhattanDistanceHeuristic(closestLambda);
            pr = new ClosestLambdaProblem(closestLambda);
            al = new AstarLogic(sim, he, pr);
            try{
                pathToClosestLambda = al.solve();
            }catch(NoSuchElementException ex){
                return this.currentSolve;
            }
            this.currentSolve = this.currentSolve.concat(pathToClosestLambda);
            this.currentState = al.getGameState();
            this.sim.setGameState(this.currentState);
            lambdaList = this.currentState.getMap().getLambdaList();
        }
        // после того как собраны все лямбды идем к лифту
        he = new ManhattanDistanceHeuristic(this.currentState.getMap().getLiftPosition());
        pr = new ClosestLambdaProblem(this.currentState.getMap().getLiftPosition());
        al = new AstarLogic(sim, he, pr);
        String pathToLift = al.solve();
        this.currentSolve = this.currentSolve.concat(pathToLift);
        this.currentState = al.getGameState();
        this.sim.setGameState(this.currentState);
        return this.currentSolve;
    }

    @Override
    public String getCurrentSolve() {
        return this.currentSolve;
    }
    
    /**
     * Определяет координаты ближайшей лямбды к роботу
     * @return координаты лямбды
     */
    private Pair defineClosestLambda(ArrayList<Pair> lambdaList, Pair robot){
        int distance = Integer.MAX_VALUE;
        Pair closestLambda = null;
        int manhDist;
        for (Pair lambda : lambdaList) {
            manhDist = manhattanDistance(robot, lambda);
            if(distance > manhDist){
                distance = manhDist;
                closestLambda = lambda;
            }
        }
        return closestLambda;
    }
    
    
    private int manhattanDistance(Pair first, Pair second){
        return Math.abs(first.getX() - second.getX()) + Math.abs(first.getY() - second.getY());
    }

    public GameState getGameState() {
        return currentState;
    }
        
}
