package com.kspt.group1.lambdaserver.logic.Astar;

/**
 * Набор стоимостей путей.
 * @author Колтон Семен 5081/14
 */
public class StepCost {
    /**
     * Единичная стоимость шага
     * @return 1
     */
    public static int SimpleOneCost(){
        return 1;
    }
}
