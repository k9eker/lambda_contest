package com.kspt.group1.lambdaserver.simulator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Симулятор.
 * @author Колтон Семен 5081/14
 */
public class Simulator {
    // карта
    private Map map;
    // игровая статистика
    private GameStatistics gameStatistics;

    /**
     * Конструктор.
     * Принимает на вход карту в виде строки, создает на ее основе объект Map
     * и инициализирует статистику игры
     * @param map строковое представление карты
     * @throws IllegalArgumentException в случае ошибки в строковом представлении карты
     */
    public Simulator(String map) throws IllegalArgumentException{
        this.map = new Map(map);
        this.gameStatistics = new GameStatistics();
    }

    /**
     * Возвращает карту.
     * @return карту для, которой производится симуляция 
     */
    public Map getMap() {
        return map;
    }

    /**
     * Устанавливает карту.
     * @param map объект карта
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * Возвращает текущую статистику игры.
     * @return статистику игры
     */
    public GameStatistics getGameStatistics() {
        return gameStatistics;
    }

    /**
     * Установить игровую статистику
     * @param gameStatistics объект игровая статистика
     */
    public void setGameStatistics(GameStatistics gameStatistics) {
        this.gameStatistics = gameStatistics;
    }
    
    /**
     * Получить состояние игры.
     * @return копию карты и игровой статистики
     */
    public GameState getGameState(){
        return new GameState(map, gameStatistics);
    }
    
    /**
     * Устанавливает состояние игры.
     * @param gameState состояние игры
     */
    public void setGameState(GameState gameState){
        this.map = gameState.getMap();
        this.gameStatistics = gameState.getGameStatistics();
    }
    
    
    /**
     * Произвести ход вверх. 
     * Робот перемещается на одну клетку вверх.
     * @return обновленную игровую статистику, из которой можно узнать успешность хода,
     * жив ли робот, информацию об очках и т.д.
     * 
     */
    public GameState moveUp(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            moveRobot(0, 1);
            if(willRobotDie()){
                this.getGameStatistics().setRobotAlive(false);
                this.getGameStatistics().setOnAir(false);
                this.getGameStatistics().setLastMoveSuccessful(false);
                this.getGameStatistics().setPoints(0);
                return this.getGameState();
            }else{
                updateMap();
            }
        }
        return this.getGameState();
    }
    
    /**
     * Произвести ход вниз. 
     * Робот перемещается на одну клетку вниз.
     * @return обновленную игровую статистику, из которой можно узнать успешность хода,
     * жив ли робот, информацию об очках и т.д.
     * 
     */
    public GameState moveDown(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            moveRobot(0, -1);
            if(willRobotDie()){
                this.getGameStatistics().setRobotAlive(false);
                this.getGameStatistics().setOnAir(false);
                this.getGameStatistics().setLastMoveSuccessful(false);
                this.getGameStatistics().setPoints(0);
                return this.getGameState();
            }else{
                updateMap();
            }
        }
        return this.getGameState();
    }
    
    /**
     * Произвести ход влево. 
     * Робот перемещается на одну клетку влево.
     * @return обновленную игровую статистику, из которой можно узнать успешность хода,
     * жив ли робот, информацию об очках и т.д.
     * 
     */
    public GameState moveLeft(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            moveRobot(-1, 0);
            if(willRobotDie()){
                this.getGameStatistics().setRobotAlive(false);
                this.getGameStatistics().setOnAir(false);
                this.getGameStatistics().setLastMoveSuccessful(false);
                this.getGameStatistics().setPoints(0);
                return this.getGameState();
            }else{
                updateMap();
            }
        }
        return this.getGameState();
    }
    
    /**
     * Произвести ход вправо. 
     * Робот перемещается на одну клетку вправо.
     * @return обновленную игровую статистику, из которой можно узнать успешность хода,
     * жив ли робот, информацию об очках и т.д.
     * 
     */
    public GameState moveRight(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            moveRobot(1, 0);
            if(willRobotDie()){
                this.getGameStatistics().setRobotAlive(false);
                this.getGameStatistics().setOnAir(false);
                this.getGameStatistics().setLastMoveSuccessful(false);
                this.getGameStatistics().setPoints(0);
                return this.getGameState();
            }else{
                updateMap();
            }
        }
        return this.getGameState();
    }
    
    /**
     * Произвести ход - ожидание. 
     * Ожидать один ход. Робот не двигается
     * @return обновленную игровую статистику, из которой можно узнать успешность хода,
     * жив ли робот, информацию об очках и т.д.
     * 
     */
    public GameState moveWait(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            gameStatistics.setPoints(gameStatistics.getPoints() - 1);
            gameStatistics.setSteps(gameStatistics.getSteps() + 1);
            gameStatistics.setLastMoveSuccessful(true);
            
            if(willRobotDie()){
                this.getGameStatistics().setRobotAlive(false);
                this.getGameStatistics().setOnAir(false);
                this.getGameStatistics().setLastMoveSuccessful(false);
                this.getGameStatistics().setPoints(0);
                return this.getGameState();
            }else{
                updateMap();
            }
        }
        return this.getGameState();
    }
    
    /**
     * Произвести ход - прервать. 
     * Прерывается работа симулятора. Игра заканчивается, подсчитываются все очки.
     * @return обновленную игровую статистику информацию об очках и т.д.
     * 
     */
    public GameState moveAbort(){
        if(gameStatistics.isOnAir() && gameStatistics.isRobotAlive()){
            gameStatistics.setLastMoveSuccessful(true);
            gameStatistics.setOnAir(false);
            gameStatistics.setPoints(gameStatistics.getPoints() + 
                                        25 * gameStatistics.getLambdaEaten());
        }
        return this.getGameState();
    }
    
    
    public GameState moveUp(GameState gameState){
        this.setGameState(gameState);
        return moveUp();
    }
    
    public GameState moveDown(GameState gameState){
        this.setGameState(gameState);
        return moveDown();
    }
    
    public GameState moveLeft(GameState gameState){
        this.setGameState(gameState);
        return moveLeft();
    }
    
    public GameState moveRight(GameState gameState){
        this.setGameState(gameState);
        return moveRight();
    }
    
    public GameState moveWait(GameState gameState){
        this.setGameState(gameState);
        return moveWait();
    }
    
    public GameState moveAbort(GameState gameState){
        this.setGameState(gameState);
        return moveAbort();
    }
    
    
    /**
     * Метод двигает робота в указанном направлении.
     * deltaX и deltaY могут быть только {-1, 0, 1}, причем один из них 
     * обязательно должен быть равен нулю.
     * Производит обновление статистики игры по позициям: положение робота, 
     * количество съеденых ямбд, количество шагов, количество очков, 
     * открыт ли лифт, успешность хода, завершена ли игра
     * @param deltaX направление по оси x.
     * @param deltaY направление по оси y
     */
    private void moveRobot(int deltaX, int deltaY){
        // создаем копии карты и статистики
        Map newMap = (Map)this.map.clone();
        GameStatistics newGameStatistics = (GameStatistics)this.gameStatistics.clone();
        // двигаем робота на копии карты
        Pair robotPosition = map.getRobotPosition();
        Pair newPosition = new Pair(robotPosition.getX() + deltaX, 
                    robotPosition.getY() + deltaY);
        Cell neighbourCell = map.getCell(newPosition);
        switch(neighbourCell){
            // если пососедству стена или закрытый лифт, то не двигаемя,
            // говорим, что ход был не успешен, изменяем количество шагов и очков
            case CLOSED_LIFT: case WALL:
                newGameStatistics.setLastMoveSuccessful(false);
                newGameStatistics.setPoints(gameStatistics.getPoints() - 1);
                newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                break;
                
            // если пососедству пустая клетка или земля то двигаем робота,
            // оставляем пустую клетку, изменяем статистику
            case FREE: case GROUND:
                newMap.setCell(robotPosition, Cell.FREE);
                newMap.setCell(newPosition, Cell.ROBOT);
                newMap.setRobotPosition(newPosition);
                newGameStatistics.setLastMoveSuccessful(true);
                newGameStatistics.setPoints(gameStatistics.getPoints() - 1);
                newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                break;
                
            // если пососедству лямбда двигаем робота, оставляем пустую клетку,
            // изменяем статистику
            // если это последняя лямбда, то изменяем закрытый лифт на открытый
            case LAMBDA:
                newMap.setCell(robotPosition, Cell.FREE);
                newMap.setCell(newPosition, Cell.ROBOT);
                newMap.setRobotPosition(newPosition);
                ArrayList<Pair> lambdaList = map.getLambdaList();
                lambdaList.remove(newPosition);
                newMap.setLambdaList(lambdaList);
                if(lambdaList.isEmpty()){
                    newGameStatistics.setLiftOpened(true);
                    newMap.setCell(map.getLiftPosition(), Cell.OPENED_LIFT);
                }
                newGameStatistics.setLambdaEaten(gameStatistics.getLambdaEaten() + 1);
                newGameStatistics.setLastMoveSuccessful(true);
                newGameStatistics.setPoints(gameStatistics.getPoints() + 24);
                newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                break;
                
            // если пососедству открытый лифт двигаем робота, 
            // оставляем пустую клетку, изменяем статистику
            case OPENED_LIFT:
                newMap.setCell(robotPosition, Cell.FREE);
                newMap.setCell(newPosition, Cell.ROBOT);
                newMap.setRobotPosition(newPosition);
                newGameStatistics.setLastMoveSuccessful(true);
                newGameStatistics.setPoints(gameStatistics.getPoints() + 
                                  gameStatistics.getLambdaEaten() * 50 - 1);
                newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                newGameStatistics.setOnAir(false);
                break;
                
            // если пососедству каvень, то проверяем можно ли его подвинуть
            // если можно, то изменяем позию робота, двигаем камень, 
            // оставляем пустую ячейку, изменяем статистику
            case STONE:
                Pair stoneNeighborPosition = new Pair(newPosition.getX() + deltaX, 
                    newPosition.getY() + deltaY);
                if(map.getCell(stoneNeighborPosition) == Cell.FREE
                        && deltaY == 0){
                    newMap.setCell(robotPosition, Cell.FREE);
                    newMap.setCell(newPosition, Cell.ROBOT);
                    newMap.setRobotPosition(newPosition);
                    newMap.setCell(stoneNeighborPosition, Cell.STONE);
                    newGameStatistics.setLastMoveSuccessful(true);
                    newGameStatistics.setPoints(gameStatistics.getPoints() - 1);
                    newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                }else{
                    newGameStatistics.setLastMoveSuccessful(false);
                    newGameStatistics.setPoints(gameStatistics.getPoints() - 1);
                    newGameStatistics.setSteps(gameStatistics.getSteps() + 1);
                }
                break;
        }      
        this.map = newMap;
        this.gameStatistics = newGameStatistics;
    }
    
    
    /**
     * Метод, обновляющий карту.
     * Обновляет статистику игры по пунктам жив ли робот, успешен ли последний ход,
     * продолжается ли игра
     */
    private void updateMap(){
        // создаем копии карты и статистики
        Map newMap = (Map)this.map.clone();
        GameStatistics newGameStatistics = (GameStatistics)this.gameStatistics.clone();
        // обновляем новую копию карты
        Cell currentCell;
        // по строкам снизу вверх
        for (int i = 1; i <= map.getMapSize().getY(); i++) {
            // по столбцам справа налево
            for (int j = 1; j <= map.getMapSize().getX(); j++) {
                // прсматриваем ячейки
                currentCell = map.getCell(j, i);
                // если камень, то двигаем его
                if(currentCell == Cell.STONE){
                    // если под камнем пусто, то он падает вниз
                    if(map.getCell(j, i-1) == Cell.FREE){
                        newMap.setCell(j, i, Cell.FREE);
                        newMap.setCell(j, i-1, Cell.STONE);
                        // если камень упал на робота то робот умер
                        if(map.getCell(j, i-2) == Cell.ROBOT){
                            newGameStatistics.setRobotAlive(false);
                            newGameStatistics.setOnAir(false);
                            newGameStatistics.setLastMoveSuccessful(false);
                            newGameStatistics.setPoints(0);
                        }
                    }
                    
                    // если под камненм камень
                    else if(map.getCell(j, i-1) == Cell.STONE){
                        // если справа и справа-снизу пусто, то камень падает
                        // по диагонали вправо
                        if((map.getCell(j+1, i) == Cell.FREE) && (map.getCell(j+1, i-1) == Cell.FREE)){
                            newMap.setCell(j, i, Cell.FREE);
                            newMap.setCell(j+1, i-1, Cell.STONE);
                            // если камень упал на робота то робот умер
                            if(map.getCell(j+1, i-2) == Cell.ROBOT){
                                newGameStatistics.setRobotAlive(false);
                                newGameStatistics.setOnAir(false);
                                newGameStatistics.setLastMoveSuccessful(false);
                                newGameStatistics.setPoints(0);
                            }
                        }
                        
                        // если слева и слева-снизу пусто, то камень падает
                        // по диагонали влево
                        else if((map.getCell(j-1, i) == Cell.FREE) && (map.getCell(j-1, i-1) == Cell.FREE)){
                            newMap.setCell(j, i, Cell.FREE);
                            newMap.setCell(j-1, i-1, Cell.STONE);
                            // если камень упал на робота то робот умер
                            if(map.getCell(j-1, i-2) == Cell.ROBOT){
                                newGameStatistics.setRobotAlive(false);
                                newGameStatistics.setOnAir(false);
                                newGameStatistics.setLastMoveSuccessful(false);
                                newGameStatistics.setPoints(0);
                            }
                        }
                    }
                    
                    // если под камнем лямбда, справа и справа-снизу пусто то камень падает направо
                    else if((map.getCell(j, i-1) == Cell.LAMBDA) && (map.getCell(j+1, i) == Cell.FREE) 
                                    && (map.getCell(j+1, i-1) == Cell.FREE)){
                        newMap.setCell(j, i, Cell.FREE);
                        newMap.setCell(j+1, i-1, Cell.STONE);
                        // если камень упал на робота то робот умер
                        if(map.getCell(j+1, i-2) == Cell.ROBOT){
                            newGameStatistics.setRobotAlive(false);
                            newGameStatistics.setOnAir(false);
                            newGameStatistics.setLastMoveSuccessful(false);
                            newGameStatistics.setPoints(0);
                        }
                    }
                }
            }
            
        }
        this.map = newMap;
        this.gameStatistics = newGameStatistics;
    }

    /**
     * Метод проверяет умрет ли робот после того как уже произведен его ход,
     * но до обновления карты.
     * @return true - умрет, false - выживет
     */
    private boolean willRobotDie(){
        try{
            int rx = this.getRobotPosition().getX();
            int ry = this.getRobotPosition().getY();
            /*#*# 
             *# #
             *#R#
             */
            if( this.map.getCell(rx, ry+1)==Cell.FREE 
                    && this.map.getCell(rx, ry+2)==Cell.STONE){
                return true;
            }
            /*
             * #####   #####
             * #*_ #   #*_ #
             * #*_ #   #\_ #
             * # R #   # R #
             * #####   #####
             */
            else if( this.map.getCell(rx-1, ry+2)==Cell.STONE
                        && ( this.map.getCell(rx-1, ry+1)==Cell.STONE || this.map.getCell(rx, ry+1)==Cell.LAMBDA )
                        && this.map.getCell(rx, ry+1)==Cell.FREE
                        && this.map.getCell(rx, ry+2)==Cell.FREE){
                return true;
            }
            /*
             * ######
             * # _*X#
             * # _*X#
             * # R  #
             * ###### 
             */
            else{ 
                if(this.getMapSize().getX() == rx+2){
                    if( this.map.getCell(rx+1, ry+2)==Cell.STONE 
                        && this.map.getCell(rx+1, ry+1)==Cell.STONE
                        && this.map.getCell(rx, ry+1)==Cell.FREE
                        && this.map.getCell(rx, ry+2)==Cell.FREE){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    if( this.map.getCell(rx+1, ry+2)==Cell.STONE 
                        && this.map.getCell(rx+1, ry+1)==Cell.STONE
                        && this.map.getCell(rx, ry+1)==Cell.FREE
                        && this.map.getCell(rx, ry+2)==Cell.FREE
                        && ( this.map.getCell(rx+2, ry+1)!=Cell.FREE || this.map.getCell(rx+2, ry+2)==Cell.FREE )){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }catch(IllegalArgumentException ex){
            return false;
        }
        
    }
    
    
    // Делегированные методы из классов Map и GameStatistics
    public Cell getCell(Pair p) throws IllegalArgumentException {
        return map.getCell(p);
    }

    public Cell getCell(int x, int y) throws IllegalArgumentException {
        return map.getCell(x, y);
    }

    public Pair getMapSize() {
        return map.getMapSize();
    }

    public Pair getLiftPosition() {
        return map.getLiftPosition();
    }

    public Pair getRobotPosition() {
        return map.getRobotPosition();
    }

    public ArrayList<Pair> getLambdaList() {
        return map.getLambdaList();
    }

    public List<Cell> getNeighbors(Pair p) throws IllegalArgumentException {
        return map.getNeighbors(p);
    }

    public List<Cell> getNeighbors(int x, int y) throws IllegalArgumentException {
        return map.getNeighbors(x, y);
    }

    public String getMapStringRepresentation() {
        return map.getMapStringRepresentation();
    }

    public int getLambdaEaten() {
        return gameStatistics.getLambdaEaten();
    }

    public int getPoints() {
        return gameStatistics.getPoints();
    }

    public int getSteps() {
        return gameStatistics.getSteps();
    }

    public boolean isLiftOpened() {
        return gameStatistics.isLiftOpened();
    }

    public boolean isRobotAlive() {
        return gameStatistics.isRobotAlive();
    }

    public boolean isLastMoveSuccessful() {
        return gameStatistics.isLastMoveSuccessful();
    }

    public boolean isOnAir() {
        return gameStatistics.isOnAir();
    }
    
    
    
}
