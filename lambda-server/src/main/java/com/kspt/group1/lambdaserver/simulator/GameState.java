/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kspt.group1.lambdaserver.simulator;

/**
 * Состояние игры. 
 * Хранит текущую карту и игровую статистику
 * @author Колдтон Семен 5081/14
 */
public class GameState {
    private Map map;
    private GameStatistics gameStatistics;

    /**
     * Конструктор. Принимает на вход игровую статистику и карту.
     * @param map текущая карта
     * @param gameStatistics текущая игровая статистика
     */
    public GameState(Map map, GameStatistics gameStatistics) {
        this.map = map;
        this.gameStatistics = gameStatistics;
    }

    public GameStatistics getGameStatistics() {
        return gameStatistics;
    }

    public Map getMap() {
        return map;
    }
    
    
   
   
}
