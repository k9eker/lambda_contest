package com.kspt.group1.lambdaserver.logic.Astar;

/**
 * Интерфейс описывающий проблему, решаемую алгоритмом
 * @author Колтон Семен 5081/14
 */
public interface Problem {
    /**
     * Метод проверяет является ли переданное ему состояние игры конечным
     * @param as состояние игры
     * @return true - конечное состояние, false - не конечное
     */
    boolean isGoalState(AstarState as);
}
