package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameStatistics;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Колтон Семен 5081/14
 */
public class MainAstar {
    public static void main( String[] args ) throws IOException, FileNotFoundException, InterruptedException{
        MainAstar ma = new MainAstar();        
    }

    public MainAstar() throws FileNotFoundException, IOException, InterruptedException {
//        for (int i = 1; i < 11; i++) {
//            String map = readFile("src/main/resources/maps/contest" + i + ".map");
//            System.out.println("contest " + i + ":");
//            AstarLogic l = new AstarLogic(map);
//            //ClosestLambdaLogic l = new ClosestLambdaLogic(map);
//            LogicThread lt = new LogicThread(l, Thread.currentThread());
//
//            // ожидаем 150 секунд
//            try{
//                Thread.sleep(150000);
//            }catch(InterruptedException ex){ }
//            
//            lt.stop();
//            
//            // берем текущее решение, дописываем "A" и завершаем работу программы
//            String solve = l.getCurrentSolve().concat("A");
//            System.out.println(solve);
//            //System.exit(0);
//        }        
        String map = readFile("src/main/resources/maps/testmap6.txt");
        AstarLogic l = new AstarLogic(map);
        //ClosestLambdaLogic l = new ClosestLambdaLogic(map);
        LogicThread lt = new LogicThread(l, Thread.currentThread());

        // ожидаем 150 секунд
        try{
            Thread.sleep(150000);
        }catch(InterruptedException ex){ }

        lt.stop();

        // берем текущее решение, дописываем "A" и завершаем работу программы
        String solve = l.getCurrentSolve().concat("A");
        System.out.println(solve);
        //System.exit(0);
    }
    
    /**
    * Метод целиком читатет файл и записывает в строку
    */
    private String readFile(String name) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while((str = br.readLine()) != null){
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }
    
    
    // поток, в котором запускается алгоритм
    class LogicThread extends Thread{
        //ClosestLambdaLogic l;
        AstarLogic l;
        Thread father;

        //public LogicThread(ClosestLambdaLogic l, Thread father) {
        public LogicThread(AstarLogic l, Thread father) {
            this.l = l;
            this.father = father;
            this.start();
        }
        
        @Override
        public void run() {
            String solve = l.solve();
            //System.out.println("Solve: " + solve + "A");
            //GameStatistics gs = l.getGameState().getGameStatistics();
            //System.out.println("Result map: \n");
            //System.out.println(l.getGameState().getMap().getMapStringRepresentation());
            //System.out.println("Robot alive:" + gs.isRobotAlive());
            //System.out.println("Points:" + gs.getPoints());
            //System.out.println("Steps:" + gs.getSteps());
            //System.out.println("Lift opened:" + gs.isLiftOpened());
            //System.out.println("Lambda eaten:" + gs.getLambdaEaten());
            //System.exit(0);
            father.interrupt();
        }
    }
}
