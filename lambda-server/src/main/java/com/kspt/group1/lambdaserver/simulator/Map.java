package com.kspt.group1.lambdaserver.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Описывает карту шахты. 
 * Хранит информацию о самой карте, о положении лямбд, 
 * положении робота, положении лифта и т.д.
 * @author Колтон Семен 5081/14
 */
public class Map implements Cloneable{
    
    // текущая карта
    // первая координата - y, вторая координата - x
    private char[][] map;
    // список координат лямбд
    private ArrayList<Pair> lambdaList;
    // положение робота
    private Pair robotPosition;
    // размер карты
    private final Pair mapSize;
    // положение лифта
    private final Pair liftPosition;
   
    
    private Map(Pair mapSize, Pair liftPosition){
        this.mapSize = mapSize;
        this.liftPosition = liftPosition;
        this.map = new char[mapSize.getY()][mapSize.getX()];
    }
    
    /**
     * Конструктор класса Map.
     * Принимает карту в виде строки map и загружает ее.
     * @param map карта представленная в виде строки
     * @throws IllegalArgumentException в случае если поданая на вход карта неправильная
     */
    public Map(String map){
        // проверяем, что в карте есть символы
        if(map.length() == 0)
            throw new IllegalArgumentException("Map must contain at least 1 symbol.");
        // разбиваем карту на две части: до пустой строки и после пустой строки
        String[] mapParts = map.split("\n\n");
        if(mapParts.length > 2)
            throw new IllegalArgumentException("Map can contain only 2 parts: map and description");
        // проверяем, что карта содержит только допустимые символы
        Cell cells[] = Cell.values(); 
        String suitableSymbols = new String();
        for (int i = 0; i < cells.length; i++) {
            suitableSymbols = suitableSymbols.concat(String.valueOf(cells[i].toChar()));
        }
        suitableSymbols = suitableSymbols.replace("\\", "\\\\");
        if(!mapParts[0].matches("^[" + suitableSymbols + "\n]*"))
            throw new IllegalArgumentException("Map can contain only symbols:" + suitableSymbols);
        
        // парсим карту
        // определяем размер карты
        String mapStr[] = mapParts[0].split("\n");
        int xSize = 0;
        int ySize = mapStr.length;
        for (String string : mapStr) {
            if(string.length() > xSize)
                xSize = string.length();
        }
        this.mapSize = new Pair(xSize, ySize);
        this.map = new char[mapSize.getY()][mapSize.getX()];
        // инициализируем карту пустыми ячейками
        for (int i=0; i<mapSize.getY(); i++) {
            for (int j=0; j<mapSize.getX(); j++) {
                this.map[i][j] = Cell.FREE.toChar();
            }
        }
        
        // копируем карту и ищем робота, лифт и лямбды
        this.lambdaList = new ArrayList<Pair>();
        int robotX = 0, robotY = 0, robotFound = 0;
        int liftX = 0, liftY = 0, liftFound = 0;
        for (int i = 0; i < mapStr.length; i++) {
            mapStr[i].getChars(0, mapStr[i].length(), this.map[mapSize.getY() - i - 1], 0);
            
            // ищем робота, лифт и лямбды
            for (int j = 0; j < mapStr[i].length(); j++) {
                // ищем робота
                if(this.map[mapSize.getY() - i - 1][j] == Cell.ROBOT.toChar()){
                    robotX = j+1;
                    robotY = mapSize.getY() - i;
                    robotFound ++;
                    continue;
                }
                // ищем лифт
                if((this.map[mapSize.getY() - i - 1][j] == Cell.CLOSED_LIFT.toChar())
                        || (this.map[mapSize.getY() - i - 1][j] == Cell.OPENED_LIFT.toChar())){
                    liftX = j+1;
                    liftY = mapSize.getY() - i;
                    liftFound ++;
                    continue;
                }
                // ищем лямбды
                if(this.map[mapSize.getY() - i - 1][j] == Cell.LAMBDA.toChar()){
                    this.lambdaList.add(new Pair(j+1, mapSize.getY() - i));
                    continue;
                }
                
            }
        }
        
        if(robotFound != 1){
            throw new IllegalArgumentException("Map must cotain exactly 1 robot.");
        }else{
            this.robotPosition = new Pair(robotX, robotY);
        }
            
        if(liftFound != 1)
            throw new IllegalArgumentException("Map must cotain exactly 1 lift.");
        else
            this.liftPosition = new Pair(liftX, liftY);
    }
     
    /**
     * Проверяет входят ли координаты в допустимый диапазон: 0 < x,y <= mapSize
     * @param p координаты
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    private void checkCoordinate(Pair p) throws IllegalArgumentException{
        int x = p.getX();
        int y = p.getY();
        checkCoordinate(x, y);
    }
    
    /**
     * Проверяет входят ли координаты в допустимый диапазон: 0 < x,y <= mapSize
     * @param x координата x
     * @param y координата y
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    private void checkCoordinate(int x, int y) throws IllegalArgumentException{
        if((x <= 0) | (y <= 0) | (x > mapSize.getX()) | (y > mapSize.getY()))
            throw new IllegalArgumentException("Coordinate must be in appropriate range: 0 < x <= mapSize, 0 < y <= mapSize"
                    + "\n x = " + x + " y = " + y);
    }
    
    
    /**
     * Метод возвращает заначение ячейки с координатами указанными в Pair
     * @param p координаты ячейки
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     * @return значение ячейки
     */
    public Cell getCell(Pair p) throws IllegalArgumentException{
        checkCoordinate(p);
        return Cell.valueOf(this.map[p.getY()-1][p.getX()-1]);
    }
    
    /**
     * Метод возвращает заначение ячейки с указанными координатами 
     * @param x координата x
     * @param y координата y
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     * @return значение ячейки
     */
    public Cell getCell(int x, int y) throws IllegalArgumentException{
        checkCoordinate(x, y);
        return Cell.valueOf(this.map[y-1][x-1]);
    }
    
    
    /**
     * Устанавливает указанное значение в указанную ячейку
     * @param p координаты ячейки
     * @param c значение, которое нужно поместить в ячейку
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    void setCell(Pair p, Cell c) throws IllegalArgumentException{
        checkCoordinate(p);
        map[p.getY()-1][p.getX()-1] = c.toChar();
    }
    
    
    /**
     * Устанавливает указанное значение в указанную ячейку
     * @param x координат x
     * @param y координат y
     * @param c значение, которое нужно поместить в ячейку
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    void setCell(int x, int y, Cell c) throws IllegalArgumentException{
        checkCoordinate(x, y);
        map[y-1][x-1] = c.toChar();
    }
   
    /**
     * Возвращает размер карты в виде объекта Pair
     * @return размер карты
     */
    public Pair getMapSize() {
        return mapSize;
    }

    /**
     * Возвращает позицию лифта в виде объекта Pair
     * @return координаты лифта
     */    
    public Pair getLiftPosition() {
        return liftPosition;
    }

    /**
     * Возвращает позицию робота в виде объекта Pair
     * @return позиция робота
     */
    public Pair getRobotPosition() {
        return robotPosition;
    }

    /**
     * Возвращает список координат всех лямбд на карте
     * @return список координат лямбд
     */
    public ArrayList<Pair> getLambdaList() {
        return (ArrayList)lambdaList.clone();
    }

    
    /**
     * Устанавливает новую позицию робота
     * @param robotPosition новая позиция робота в виде объекта Pair
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    void setRobotPosition(Pair robotPosition) throws IllegalArgumentException{
        checkCoordinate(robotPosition);
        this.robotPosition = robotPosition;
    }

   
    /**
     * Устанавливает список лямбд
     * @param lambdaList список лямбд
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    void setLambdaList(ArrayList<Pair> lambdaList) throws IllegalArgumentException{
        for (Pair pair : lambdaList) {
            checkCoordinate(pair);
        }
        this.lambdaList = (ArrayList)lambdaList.clone();
    }
    
    
    /**
     * Возвращает список соседей указанной клетки. 
     * Первой указывается сама ячейка, а затем соседние ячейки по часовой 
     * стрелке, начиная с левой нижней
     * <br>3 4 5
     * <br>2 0 6
     * <br>1 8 7
     * @param p 
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    public List<Cell> getNeighbors(Pair p) throws IllegalArgumentException{
        checkCoordinate(p);
        return getNeighbors(p.getX(), p.getY());
    }
    
    /**
     * Возвращает список соседей указанной клетки. 
     * Первой указывается сама ячейка, а затем соседние ячейки по часовой 
     * стрелке, начиная с левой нижней
     * <br>3 4 5
     * <br>2 0 6
     * <br>1 8 7
     * @param x координата x
     * @param x координата y
     * @throws IllegalArgumentException в случае если координаты не входят в допустимый диапазон
     */
    public List<Cell> getNeighbors(int x, int y) throws IllegalArgumentException{
        checkCoordinate(x, y);
        ArrayList<Cell> neighborsList = new ArrayList<Cell>(9);
        for (Cell cell : neighborsList) {
            cell = Cell.FREE;
        }
        neighborsList.add(0, Cell.valueOf(map[y][x]));
        if(x == 1){
            if(y == 1){
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
                neighborsList.add(5, Cell.valueOf(map[y+1][x+1]));
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
            }else if(y == mapSize.getY()){
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
                neighborsList.add(7, Cell.valueOf(map[y-1][x+1]));
                neighborsList.add(8, Cell.valueOf(map[y-1][x]));
            }else{
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
                neighborsList.add(5, Cell.valueOf(map[y+1][x+1]));
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
                neighborsList.add(7, Cell.valueOf(map[y+1][x]));
                neighborsList.add(8, Cell.valueOf(map[y+1][x+1]));
            }
        }else if(x == mapSize.getX()){
            if(y == 1){
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(3, Cell.valueOf(map[y+1][x-1]));
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
            }else if(y == mapSize.getY()){
                neighborsList.add(1, Cell.valueOf(map[y-1][x-1]));
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(8, Cell.valueOf(map[y-1][x]));
            }else{
                neighborsList.add(1, Cell.valueOf(map[y-1][x-1]));
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(3, Cell.valueOf(map[y+1][x-1]));
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
                neighborsList.add(8, Cell.valueOf(map[y-1][x]));
            }
        }else{
            if(y == 1){
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(3, Cell.valueOf(map[y+1][x-1]));
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
                neighborsList.add(5, Cell.valueOf(map[y+1][x+1]));
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
            }else if(y == mapSize.getY()){
                neighborsList.add(1, Cell.valueOf(map[y-1][x-1]));
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
                neighborsList.add(7, Cell.valueOf(map[y-1][x+1]));
                neighborsList.add(8, Cell.valueOf(map[y-1][x]));
            }else{
                neighborsList.add(1, Cell.valueOf(map[y-1][x-1]));
                neighborsList.add(2, Cell.valueOf(map[y][x-1]));
                neighborsList.add(3, Cell.valueOf(map[y+1][x-1]));
                neighborsList.add(4, Cell.valueOf(map[y+1][x]));
                neighborsList.add(5, Cell.valueOf(map[y+1][x+1]));
                neighborsList.add(6, Cell.valueOf(map[y][x+1]));
                neighborsList.add(7, Cell.valueOf(map[y-1][x+1]));
                neighborsList.add(8, Cell.valueOf(map[y-1][x]));
            }
        }
        return neighborsList;
    }
    
    
    /**
     * Метод возвращает представление карты в виде единой строки
     * @return 
     */
    public String getMapStringRepresentation(){
        String representation = new String();
        for (int i = mapSize.getY() - 1; i >= 0; i--) {
            representation = representation.concat(String.valueOf(map[i]) + '\n');
        }
        return representation;
    }
    
    /**
     * Сравнивает две карты.
     * @param obj карта с которой нужно сравнить данную
     * @return true - если равны, false - если не равны
     */
    @Override
    public boolean equals(Object obj) {
        if((obj instanceof Map) &&
                testLambdaListsEquals(((Map)obj).lambdaList, this.lambdaList) &&
                ((Map)obj).liftPosition.equals(this.liftPosition) &&
                ((Map)obj).mapSize.equals(this.mapSize) &&
                ((Map)obj).robotPosition.equals(this.robotPosition) &&
                testMapsArrayEquals(((Map)obj).map, this.map))
            return true;
        else return false;
    }
   
    private boolean testLambdaListsEquals(ArrayList<Pair> expected, ArrayList<Pair> real){
        if (expected.containsAll(real) &&real.containsAll(expected))
            return true;
        else
            return false;
    }
    
    /**
     * Метод сравнивает два двухмерных массива char
     */
    private boolean testMapsArrayEquals(char[][] map1, char[][] map2){
        boolean result = true;
        if (map1.length != map2.length)
            return false;
        for (int i = 0; i < map1.length; i++) {
            result &= Arrays.equals(map1[i], map2[i]);
        }
        return result;
    }

    /**
     * Копирует карту.
     * @return копию карты
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone(){
        Map newMap = new Map(this.mapSize, this.liftPosition);
        newMap.setLambdaList(this.lambdaList);
        newMap.setRobotPosition(this.robotPosition);
        for (int i = 0; i < this.mapSize.getX(); i++) {
            for (int j = 0; j < this.mapSize.getY(); j++) {
                newMap.setCell(i+1, j+1, this.getCell(i+1, j+1));    
            }
            
        }
        return newMap;
    }
    
    
    
    
    
}
