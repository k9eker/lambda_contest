package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;

/**
 * Эвристики.
 * @author Колтон Семен 5081/14
 */
interface Heuristic {
    /**
     * Получить эвристику для указанного состояния игры
     * @param gamestate состояние игры
     * @return эвристику в виде положительного целого числа
     */
    int getHeuristic(GameState gamestate);
    
}
