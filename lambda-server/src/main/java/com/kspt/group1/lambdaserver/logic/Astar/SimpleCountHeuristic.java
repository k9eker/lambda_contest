package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;

/**
 * Простая эвристика подсчитывающая количество оставшихся лямбд
 * @author Колтон Семен 5081/14
 */
public class SimpleCountHeuristic implements Heuristic{

    public SimpleCountHeuristic() {
    }
        
    /**
     * Простая эвристика подсчитывающая количество оставшихся лямбд
     * @param state состояние игры
     * @return вовращает положительное целое число показывающее количество оставшихся лямбд
     */
    @Override
    public int getHeuristic(GameState state){
        return state.getMap().getLambdaList().size();
    }
}
