package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Pair;

/**
 * Эвристика - манхэттенское расстояние до указанной точки
 * @author Колтон Семен 5081/14
 */
public class ManhattanDistanceHeuristic implements Heuristic{

    // целевая точка
    private Pair target;

    public ManhattanDistanceHeuristic(Pair target) {
        this.target = target;
    }

    @Override
    public int getHeuristic(GameState gamestate) {
        Pair robot = gamestate.getMap().getRobotPosition();
        return Math.abs(robot.getX() - this.target.getX()) + Math.abs(robot.getY() - this.target.getY());
    }
    
    
    
}
