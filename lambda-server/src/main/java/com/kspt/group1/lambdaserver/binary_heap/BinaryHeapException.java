package com.kspt.group1.lambdaserver.binary_heap;

/**
 * Исключение, бросаемой бинароной кучей.
 */
public class BinaryHeapException extends RuntimeException {

    public BinaryHeapException(String message) {
        super(message);
    }
}