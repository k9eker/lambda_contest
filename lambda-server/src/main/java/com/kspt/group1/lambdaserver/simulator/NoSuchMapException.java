package com.kspt.group1.lambdaserver.simulator;

/**
 * Исключение бросаемое конструктором класса Map в случае если не найдена
 * карта указанная в идентификаторе.
 * @author Колтон Семен 5081/14
 */
public class NoSuchMapException extends Exception {
    
    /**
     * Creates a new instance of
     * <code>NoSuchMapException</code> without detail message.
     */
    public NoSuchMapException() {
    }

    /**
     * Constructs an instance of
     * <code>NoSuchMapException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public NoSuchMapException(String msg) {
        super(msg);
    }
}
