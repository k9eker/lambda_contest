package com.kspt.group1.lambdaserver.simulator;

/**
 * Описывает одну ячейку поля.
 * Типы ячеек: робот, лямбда, закрытый лифт, открытый лифт, 
 * камень, земля, стена, пустота 
 * @author Семен
 */
public enum Cell {
    
    /** Робот - 'R' */
    ROBOT ('R'),    
    /** Лямбда - '\' */
    LAMBDA ('\\'),
    /** Закрытый лифт - 'L' */
    CLOSED_LIFT ('L'),
    /** Открытый лифт - 'O' */
    OPENED_LIFT ('O'),
    /** Камень - '*' */
    STONE ('*'),
    /** Земля - '*' */
    GROUND ('.'),
    /** Стена - '#' */
    WALL ('#'),
    /** Пустая клутка - ' ' */
    FREE (' ');

    //тип клетки представленный в виде символа
    private final char type;
    
    //конструктор
    private Cell(char type) {
        this.type = type;
    }
    
    /**
     * Преобразует элемент перечисления Cell в символ char.
     * @return символ char соответствующий этой ячейке 
     */
    public char toChar(){
        return this.type;
    }

    /**
     * Преобразовывает символ char в объект перечисления Cell
     * @param type char символ соответствующий ячейке
     * @return объект перечисления Cell, или <b>null</b> если соответствующий
     * элемент перечисления не найден
     */
    public static Cell valueOf(char type){
        for (Cell c : values()) {
            if (c.type == type)
                return c;
        }
        return null;
    }
}
