package com.kspt.group1.lambdaserver;
/**
 * @author Garik
 *
 */
public class Server 
{
    public static void main( String[] args )
    {
        try {
            LambdaServerApplication server = new LambdaServerApplication();
            server.start();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}