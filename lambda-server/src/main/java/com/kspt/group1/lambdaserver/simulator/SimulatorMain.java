package com.kspt.group1.lambdaserver.simulator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Колтон Семен 5081/14
 */
class SimulatorMain {

    public static void main( String[] args ) throws IOException{
        //String interactiveMap = readFile("src/main/resources/maps/testmap3.txt");
        String interactiveMap = readFile("src/main/resources/maps/testmap6.txt");
        Simulator sim = new Simulator(interactiveMap);
        GameStatistics gs;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        char c = ' ';
        boolean exit = false;
        
        gs = sim.getGameStatistics();
        System.out.println(sim.getMapStringRepresentation());
        System.out.println("Robot alive:" + gs.isRobotAlive());
        System.out.println("Points:" + gs.getPoints());
        System.out.println("Steps:" + gs.getSteps());
        System.out.println("Lift opened:" + gs.isLiftOpened());
        System.out.println("Lambda eaten:" + gs.getLambdaEaten());
        while(!exit){
            c = br.readLine().charAt(0);
            switch(c){
                case 'R': case 'r':
                    sim.moveRight();
                    break;

                case 'L': case 'l':
                    sim.moveLeft();
                    break;

                case 'U': case 'u':
                    sim.moveUp();
                    break;

                case 'D': case 'd':
                    sim.moveDown();
                    break;

                case 'W': case 'w':
                    sim.moveWait();
                    break;

                case 'A': case 'a':
                    sim.moveAbort();
                    break;

                case 'E': case 'e':
                    exit = true;
                    break;
                    
                default:
                    break;
            }
            gs = sim.getGameStatistics();
            System.out.println(sim.getMapStringRepresentation());
            System.out.println("Robot alive:" + gs.isRobotAlive());
            System.out.println("Points:" + gs.getPoints());
            System.out.println("Steps:" + gs.getSteps());
            System.out.println("Lift opened:" + gs.isLiftOpened());
            System.out.println("Lambda eaten:" + gs.getLambdaEaten());
        }
        
        br.close();
    }
    
    /**
     * Метод целиком читатет файл и записывает в строку
     */
    private static String readFile(String name) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while((str = br.readLine()) != null){
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }
}
