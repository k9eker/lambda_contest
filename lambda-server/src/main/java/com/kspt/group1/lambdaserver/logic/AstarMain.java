package com.kspt.group1.lambdaserver.logic;

import com.kspt.group1.lambdaserver.simulator.GameStatistics;
import com.kspt.group1.lambdaserver.simulator.Simulator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Илья
 */
public class AstarMain {

    public static void main(String[] args) throws IOException {
        String map = readFile("src/main/resources/maps/testmap3.txt");
        doWork(map);
    }

    /**
     * Возвращает объект типа simulator.Map
     *
     * @param map местоположение карты для загрузки
     * @return заполненный объект типа simulator.Map
     * @throws IOException, если произошла ошибка чтения файла.
     */
    private static String readFile(String map) throws IOException {
        FileReader fis = new FileReader(map);
        BufferedReader reader = new BufferedReader(fis);

        StringBuilder buffer = new StringBuilder();

        String temp = new String();
        while ((temp = reader.readLine()) != null) {
            if (temp.equals("")) {
                buffer.append("\n");
            }
            buffer.append(temp).append("\n");
        }
        reader.close();
        String mapString = buffer.toString();

        return mapString;
    }

    /**
     * Выполняет инициализацию и запуск алгоритма А*, а также осуществляет
     * движение робота от старта к цели (задание старта и цели производится в
     * конструкторе AstarMap.
     *
     * @param map карта, прочитанная из файла
     * @throws IOException, если произошла ошибка чтения файла
     */
    private static void doWork(String map) throws IOException {
        Simulator sim = new Simulator(map);
        GameStatistics gs = sim.getGameStatistics();
        AstarMap astar_map = new AstarMap(map);
        AstarLogic astar = new AstarLogic(astar_map);
        String solve = astar.solve();
        System.out.println("Path: " + solve);

        int i = 0;
        char c = ' ';
        while ((c = solve.charAt(i++)) != ' ') {
            switch (c) {
                case 'R':
                case 'r':
                    sim.moveRight();
                    break;

                case 'L':
                case 'l':
                    sim.moveLeft();
                    break;

                case 'U':
                case 'u':
                    sim.moveUp();
                    break;

                case 'D':
                case 'd':
                    sim.moveDown();
                    break;

                case 'W':
                case 'w':
                    sim.moveWait();
                    break;

                case 'A':
                case 'a':
                    sim.moveAbort();
                    break;

                case 'E':
                case 'e':
                    break;

                default:
                    break;
            }
            gs = sim.getGameStatistics();
            System.out.println(sim.getMapStringRepresentation());
            System.out.println("Robot alive:" + gs.isRobotAlive());
            System.out.println("Points:" + gs.getPoints());
            System.out.println("Steps:" + gs.getSteps());
            System.out.println("Lift opened:" + gs.isLiftOpened());
            System.out.println("Lambda eaten:" + gs.getLambdaEaten());
        }
    }
}