package com.kspt.group1.lambdaserver.simulator;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс описывающий статистику игры.
 * Хранит информацию о количестве очков, количестве съеденых лямбд,
 * количестве шагов, состоянии лифта и состоянии робота
 * @author Колтон Семен 5081/14
 */
public class GameStatistics implements Cloneable{
    // состояние робота: true - жив, false - мертв
    private boolean robotAlive;
    // количество съеденых лямбд
    private int lambdaEaten;
    // количество сделаных шагов
    private int steps;
    // количество заработанных очков
    private int points;
    // состояние лифта: true - открыт, false - закрыт 
    private boolean liftOpened;
    // успешен ли был последний ход: true - был успешен, false - был не успешен
    private boolean lastMoveSuccessful;
    // продолжается ли игра: true - игра продолжается, false - игра завершена 
    private boolean onAir;

    /**
     * Конструктор.
     * Инициализирует все параметры.
     */
    public GameStatistics() {
        robotAlive = true;
        liftOpened = false;
        points = 0;
        steps = 0;
        lambdaEaten = 0;
        lastMoveSuccessful = true;
        onAir = true;
    }

    /**
     * @return количество съеденых лямбд
     */
    public int getLambdaEaten() {
        return lambdaEaten;
    }

    /**
     * @return количество очков на данный момент
     */
    public int getPoints() {
        return points;
    }

    /**
     * @return количество произведенных шагов 
     */
    public int getSteps() {
        return steps;
    }

    /**
     * Открыт ли лифт
     * @return true - открыт, false - закрыт 
     */
    public boolean isLiftOpened() {
        return liftOpened;
    }

    /**
     * Жив ли робот.
     * @return true - жив, false - мертв
     */
    public boolean isRobotAlive() {
        return robotAlive;
    }

    /**
     * Был ли последний ход успешен.
     * @return true - был успешен, false - был не успешен
     */
    public boolean isLastMoveSuccessful() {
        return lastMoveSuccessful;
    }

    /**
     * Продолжается ли игра: true - игра продолжается, false - игра завершена 
     * @return true - игра продолжается, false - игра завершена 
     */
    public boolean isOnAir() {
        return onAir;
    }

    void setLambdaEaten(int lambdaEaten) {
        this.lambdaEaten = lambdaEaten;
    }

    void setLiftOpened(boolean liftOpened) {
        this.liftOpened = liftOpened;
    }

    void setPoints(int points) {
        this.points = points;
    }

    void setRobotAlive(boolean robotAlive) {
        this.robotAlive = robotAlive;
    }

    void setSteps(int steps) {
        this.steps = steps;
    }

    void setLastMoveSuccessful(boolean lastMoveSuccessful) {
        this.lastMoveSuccessful = lastMoveSuccessful;
    }

    void setOnAir(boolean onAir) {
        this.onAir = onAir;
    }

    /**
     * Сравнение двух статистик игры
     * @param obj объект с которым нужно сравнить
     * @return true - если равны, false - если не равны
     */
    @Override
    public boolean equals(Object obj) {
        if((obj instanceof GameStatistics) && 
                (((GameStatistics)obj).lastMoveSuccessful == this.lastMoveSuccessful) &&
                (((GameStatistics)obj).liftOpened == this.liftOpened) && 
                (((GameStatistics)obj).onAir == this.onAir) && 
                (((GameStatistics)obj).robotAlive == this.robotAlive) && 
                (((GameStatistics)obj).lambdaEaten == this.lambdaEaten) && 
                (((GameStatistics)obj).points == this.points) && 
                (((GameStatistics)obj).steps == this.steps))
            return true;
        else
            return false;
    }

    
    /**
     * Копирует игровую статистику.
     * @return копия игровой статистики.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone(){
        GameStatistics cloned = null;
        try {
            cloned = (GameStatistics)super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(GameStatistics.class.getName()).log(Level.SEVERE, null, ex);
        }
        cloned.lambdaEaten = this.lambdaEaten;
        cloned.lastMoveSuccessful = this.lastMoveSuccessful;
        cloned.liftOpened = this.liftOpened;
        cloned.onAir = this.onAir;
        cloned.points = this.points;
        cloned.robotAlive = this.robotAlive;
        cloned.steps = this.steps;
        return cloned;
    }
    
    
    
    
    
    
}
