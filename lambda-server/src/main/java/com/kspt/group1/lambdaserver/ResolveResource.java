/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kspt.group1.lambdaserver;
import com.kspt.group1.lambdaserver.logic.Astar.AstarLogic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Garik
 */
public class ResolveResource extends ServerResource{
    /**
     * @return решение карты. Если за 150 сек решатель не ответит то метод выдаст текущее решение виде String
     * @throws IOException
     * @throws ResourceException
     * @throws Exception 
     * //Для того чтобы получить решение карты testmap6.txt нужно
     * ввести в браузер: http://localhost:8182/solve/testmap6.txt 
     */    
    @Get
    public String retrieve() throws IOException, ResourceException, Exception{
        String query = getReference().getRemainingPart();
        String map = readFile("src/main/resources/maps" + query);
        AstarLogic l = new AstarLogic(map);
        LogicThread lt = new LogicThread(l, Thread.currentThread());
            // ожидаем 150 секунд
            try{
                Thread.sleep(150000);
            }catch(InterruptedException ex){ }
            lt.stop();   
            // берем текущее решение, дописываем "A" и завершаем работу программы
            String solve = l.getCurrentSolve().concat("A");
        return solve;
    }    
    // поток, в котором запускается алгоритм
    class LogicThread extends Thread{
        //ClosestLambdaLogic l;
        AstarLogic l;
        Thread father;

        //public LogicThread(ClosestLambdaLogic l, Thread father) {
        public LogicThread(AstarLogic l, Thread father) {
            this.l = l;
            this.father = father;
            this.start();
        }
        
        @Override
        public void run() {
            String solve = l.solve();
            father.interrupt();
        }
    }
    public static String readFile(String name) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while ((str = br.readLine()) != null) {
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }
}
