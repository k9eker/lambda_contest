package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.logic.Logic;
import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Simulator;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

/**
 *
 * @author Колтон Семен 5081/14
 */
public class AstarLogic implements Logic{
    
    // симулятор
    private Simulator sim; 
    // список открытых вершин
    private PriorityQueue<AstarState> fringe = new PriorityQueue<AstarState>(1, 
            new AstarStateComparator());
    // текущее состояние
    private AstarState currentState;
    // игровое состояние соответствующее лучшему текущему пути
    private AstarState bestCurrentState;
    // проблема
    private Problem problem;
    
    private HashSet<AstarState> closedSet = new HashSet<AstarState>();
    
    /**
     * Реализует обход роботом карты на основе алгоритма A*.
     * @param map карта в виде строки
     * @return маршрут робота в виде строки
     */
    @Override
    public String solve(){
        int count = 0;  // счетчик количества шагов, после 10-ти шаов очищается closedset
        try{
            currentState = fringe.remove();
            while(!problem.isGoalState(currentState)){
                for (AstarState astarState : currentState.getSuccessors(sim)) {
                    if(!closedSet.contains(astarState)){
                        fringe.add(astarState);
                        closedSet.add(astarState);

                        if(currentState.getScore() > bestCurrentState.getScore()){
                            bestCurrentState = currentState;
                        }
                    }
                }
                
                currentState = fringe.remove();

                if(count == 10){
                    closedSet.clear();
                }
                count++;

            }
        }catch(NoSuchElementException ex){
            return bestCurrentState.getPath();
        }
        bestCurrentState = currentState;
        return bestCurrentState.getPath();        
    }

    @Override
    synchronized public String getCurrentSolve(){
        return bestCurrentState.getPath();
    }
    
    
    /**
     * Конструктор AstartLogic.
     * @param map карта в виде строки
     */
    public AstarLogic(String map) {
        // по умолчанию используется LiftProblem
        problem = new LiftProblem();
        sim = new Simulator(map);
        GameState gameState = sim.getGameState();
        // установка типа эвристики
        AstarState.heuristic = new ClosestPlusNumberHeuristic(sim.getMap());
        //AstarState.heuristic = new SpanningTreeHeuristic();
        currentState = new AstarState(gameState, "", 0);
        fringe.add(currentState);
        // установка текущего пути и количества очков
        bestCurrentState = currentState;
    }
    
    /**
     * Конструктор AstartLogic.
     * Принимает на вход решаемую проблему
     * @param map карта в виде строки
     * @param pr решаемая проблема
     */
    AstarLogic(Simulator sim, Heuristic he, Problem pr) {
        // установка типа эвристики
        AstarState.heuristic = he;
        this.problem = pr;
        this.sim = sim;
        this.currentState = new AstarState(sim.getGameState(), "", 0);
        this.fringe.add(currentState);
        // установка текущего пути и количества очков
        this.bestCurrentState = this.currentState;
    }
    
    public GameState getGameState(){
        return currentState.getGameState();
    }
   
    
}
