package com.kspt.group1.lambdaserver.logic;

/**
 *
 * @author Илья
 */
public class AstarCell {

    protected int x, y;
    private static final byte BLOCKED = 0x7F;
    private byte cost;
    private AstarCell next_astar_cell;

    private AstarCell() {
    }

    public AstarCell(int x, int y, int cost) {
        this.x = x;
        this.y = y;
        this.cost = (byte) cost;
        next_astar_cell = null;
    }

    public void block() {
        setCost(BLOCKED);
    }

    public void clearPathFlag() {
        cost &= 0x7F;
    }

    public AstarCell clone() {
        AstarCell astar_cell = new AstarCell();

        astar_cell.x = x;
        astar_cell.y = y;
        astar_cell.cost = cost;
        astar_cell.next_astar_cell = next_astar_cell;

        return astar_cell;
    }

    public int getCost() {
        return (cost & 0x7F);
    }

    public AstarCell getNextAstarCell() {
        return next_astar_cell;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isBlocked() {
        return getCost() == BLOCKED;
    }

    public boolean isPathFlagOn() {
        return (cost & 0x80) != 0;
    }

    public void setCost(int cost) {
        if (cost > 0x7F) {
            throw new IllegalArgumentException();
        }
        this.cost = (byte) cost;
    }

    public void setNextAstarCell(AstarCell astar_cell) {
        next_astar_cell = astar_cell;
    }

    public void setPathFlag() {
        cost |= 0x80;
    }

    public String toString() {
        int aux = x + 1;
        return new String((aux < 10 ? " " : "") + Integer.toString(aux) + Character.toString((char) (y + 'A')));
    }
}