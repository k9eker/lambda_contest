package com.kspt.group1.lambdaserver.logic;

import com.kspt.group1.lambdaserver.binary_heap.BinaryHeap;
import com.kspt.group1.lambdaserver.binary_heap.BinaryHeapElement;

/**
 * Основной класс алгоритма А*, реализующий его основную логику.
 *
 * @author Илья
 */
public class AstarLogic {

    // Список "открытых" (необработанных) узлов
    private BinaryHeap open_list;
    private int w, h, path_cost, neighborhood;
    private boolean has_solution;
    private AstarNode graph[][], goal, start;

    public AstarLogic(AstarMap map) {
        w = map.getMapSize().getX();
        h = map.getMapSize().getY();
        neighborhood = map.getNeighbors(map.getRobotPosition()).size();

        open_list = new BinaryHeap(w * h);

        graph = new AstarNode[w][h];

        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                graph[x][y] = new AstarNode(map.getAstarCell(x, y));
                graph[x][y].h = distanceToGoal(graph[x][y].getAstarCell(), map.getGoal());
            }
        }
        goal = graph[map.getGoal().getX()][map.getGoal().getY()];
        start = graph[map.getStart().getX()][map.getStart().getY()];

        System.out.println("Start: (" + (start.x + 1) + "," + (start.y + 1) + ")");
        System.out.println("Goal: (" + (goal.x + 1) + "," + (goal.y + 1) + ")");
        has_solution = false;

        //Заполняем свойства вершины start
        start.parent = null;
        start.g = start.getAstarCell().getCost(); // g(x). Стоимость пути от начальной вершины. У start g(x) = 0
        start.h = distanceToGoal(start.getAstarCell(), goal.getAstarCell()); // h(x). Эвристическая оценка расстояние до цели
        start.f = start.g + start.h;           // f(x) = g(x) + h(x)
        open_list.insert(start);
    }

    /**
     * @param start начальный узел.
     * @param goal целевой узел.
     * @return эвристику как манхэттенское расстояние.
     */
    private int distanceToGoal(AstarCell start, AstarCell goal) {
        return Math.abs(start.x - goal.x) + Math.abs(start.y - goal.y);
    }

    /**
     * Основной метод, реализующий алгоритм.
     *
     * @return
     */
    public String solve() {
        StringBuilder coordinates = new StringBuilder();
        StringBuilder solve = new StringBuilder();
        AstarNode node;
        while (!hasExecutionFinished()) {
            node = (AstarNode) open_list.pop();
            node.closed = true;

            if (node == goal) {
                AstarNode node_child = null;
                path_cost = node.g;
                has_solution = true;

                node.getAstarCell().setPathFlag();
                node_child = node;
                coordinates.append("(").append(node.x + 1).append(",").append(node.y + 1).append(")");
                if ((node.x - node.parent.x) == 1) {
                    solve.append(AstarMap.moves[2]);
                } else if ((node.x - node.parent.x) == -1) {
                    solve.append(AstarMap.moves[6]);
                } else if ((node.y - node.parent.y) == 1) {
                    solve.append(AstarMap.moves[4]);
                } else if ((node.y - node.parent.y) == -1) {
                    solve.append(AstarMap.moves[8]);
                }
                node = node.parent;

                while (node != null) {
                    coordinates.append("(").append(node.x + 1).append(",").append(node.y + 1).append(")");
                    node.getAstarCell().setNextAstarCell(node_child.getAstarCell());
                    node.getAstarCell().setPathFlag();
                    node_child = node;
                    if (node.parent != null) {
                        if ((node.x - node.parent.x) == 1) {
                            solve.append(AstarMap.moves[2]);
                        } else if ((node.x - node.parent.x) == -1) {
                            solve.append(AstarMap.moves[6]);
                        } else if ((node.y - node.parent.y) == 1) {
                            solve.append(AstarMap.moves[4]);
                        } else if ((node.y - node.parent.y) == -1) {
                            solve.append(AstarMap.moves[8]);
                        }
                    }
                    node = node.parent;
                }
                break;
            }

            for (int i = 0; i < neighborhood; i++) {
                int x, y;
                x = node.getAstarCell().getX() + AstarMap.delta_x[i];
                y = node.getAstarCell().getY() + AstarMap.delta_y[i];
                if (0 <= x && x < w && 0 <= y && y < h) {
                    AstarNode child = graph[x][y];
                    if (child.getAstarCell().isBlocked() || child.closed) {
                        continue;
                    }
                    int cost = child.getAstarCell().getCost();

                    if (open_list.has(child)) {
                        if (child.g > node.g + cost) {
                            child.parent = node;
                            child.g = node.g + cost;
                            child.f = child.g + child.h;
                            open_list.insert(child);
                        }
                    } else {
                        child.parent = node;
                        child.g = node.g + cost;
                        child.f = child.g + child.h;
                        open_list.insert(child);
                    }
                }
            }
        }
        return solve.toString() + " " + coordinates.toString();
    }

    public String getOpenListText() {
        String s = new String();

        for (int i = 0; i < open_list.size(); i++) {
            s += ((AstarNode) open_list.getElement(i)).toString() + "\n";
        }
        return s;
    }

    public int getPathCost() {
        return path_cost;
    }

    public int getAstarCellG(AstarCell astar_cell) {
        return graph[astar_cell.getY()][astar_cell.getX()].g;
    }

    public int getAstarCellH(AstarCell astar_cell) {
        return graph[astar_cell.getY()][astar_cell.getX()].h;
    }

    public boolean hasExecutionFinished() {
        return open_list.size() == 0 || goal.closed;
    }

    public boolean hasSolution() {
        return has_solution;
    }

    public boolean isInOpenList(AstarCell astar_cell) {
        return open_list.has(graph[astar_cell.getY()][astar_cell.getX()]);
    }

    public boolean isInClosedList(AstarCell astar_cell) {
        return graph[astar_cell.getY()][astar_cell.getX()].closed;
    }

    /**
     * Класс, реализующий узел на карте расчётов. Содержит координаты узла,
     * оценки для расчёта по алгоритму А*, ссылку на родительский узел.
     *
     * @author Илья
     */
    private class AstarNode extends BinaryHeapElement {

        private AstarCell astar_cell;
        // Координаты клетки
        public int x;       // координата по ширине карты
        public int y;       // координата по высоте карты
        // Оценки для алгоритма А*
        public int g;       // стоимость достижения рассматриваемого узла из начального
        public int h;       // эвристическая оценка расстояния от рассматриваемого узла к конечному
        public int f;       // стоимость как "расстояние + стоимость" (f = g + h)
        // "Родительская" клетка
        public AstarNode parent;
        // Отметка 
        public boolean closed;

        /**
         * Конструктор по умолчанию класса AstarNode.
         */
        private AstarNode() {
        }

        /**
         * Конструктор класса AstarNode с заполнением astar_cell и координат
         * клетки на карте.
         */
        public AstarNode(AstarCell astar_cell) {
            closed = false;
            this.x = astar_cell.getX();
            this.y = astar_cell.getY();
            this.astar_cell = astar_cell;
        }

        /**
         * Возвращает клетку алгоритма А*.
         *
         * @return клетку алгоритма А*.
         */
        public AstarCell getAstarCell() {
            return astar_cell;
        }

        /**
         * Преобразовывает информацию, содержащуюся в AstarNode в строковое
         * представление
         *
         * @return строковое представление AstarNode
         */
        public String toString() {
            return astar_cell.toString() + " : [" + Integer.toString(f) + ","
                    + Integer.toString(g) + "," + Integer.toString(h) + "]";
        }

        @Override
        public boolean LessThanForHeap(BinaryHeapElement e) {
            if (f == ((AstarNode) e).f) {
                return false;
            }
            return f < ((AstarNode) e).f;
        }
    }
}