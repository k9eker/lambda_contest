package com.kspt.group1.lambdaserver.main;

import com.kspt.group1.lambdaserver.logic.Astar.AstarLogic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import sun.misc.Signal;
import sun.misc.SignalHandler;

/*
 * Main-класс проекта
 */
public class Main {
    public static void main(String[] args) throws IOException {
        final AstarLogic al = new AstarLogic(readFile());
        String solve = null;
        SignalHandler signalHandler = new SignalHandler() {
            
            @Override
            public void handle(Signal sig) {
                // переводим строку, берем текущее решение, дописываем "A" 
                // и завершаем работу программы
                System.out.println("\n" + al.getCurrentSolve().concat("A"));
            }
        };
        // Установим обработчик сигнала
        InterruptSignalHandler.install("INT", signalHandler, al);
        solve = al.solve();
        System.out.println(solve);
    }
    /**
    * Метод целиком читатет файл и записывает в строку
    */
    private static String readFile() throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String result = new String();
        String str = new String();
        while((str = reader.readLine()) != null) {
            result = result.concat(str + '\n');
        }
        reader.close();
        return result;
    }
}