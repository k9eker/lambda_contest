package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.Map;

/**
 * Проблема ищущая путь до лифта.
 * Конечным состоянием является состояние, при котором робот находиться в открытом лифте
 * @author Колтон Семен 5081/14
 */
public class LiftProblem implements Problem{

    public LiftProblem() {
    }

    /**
     * Проверяет является ли состояние целевымм.
     * Состояне является конечным если робот дошел до лифта.
     * @return true - целевое, false - еще нет
     */
    @Override
    public boolean isGoalState(AstarState as) {
        Map map = as.getGameState().getMap();
        return map.getRobotPosition().equals(map.getLiftPosition());
    }
    
}
