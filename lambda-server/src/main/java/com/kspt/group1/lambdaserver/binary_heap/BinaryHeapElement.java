package com.kspt.group1.lambdaserver.binary_heap;

/*
 * Элемент бинарной кучи.
 */
public abstract class BinaryHeapElement {

    int binary_heap_index;

    public BinaryHeapElement() {
        binary_heap_index = 0;
    }

    public abstract boolean LessThanForHeap(BinaryHeapElement e);
}