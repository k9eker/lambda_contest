package com.kspt.group1.lambdaserver.simulator;

/**
 * Пара чисел.
 * @author Семен
 */
public class Pair {
    private int x;
    private int y;

    /**
     * Конструктор
     * @param x первое число
     * @param y второе число
     */
    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return первое число
     */
    public int getX() {
        return x;
    }

    /**
     * @return второе число
     */
    public int getY() {
        return y;
    }

    /**
     * Устанавливает новое значение первого числа
     * @param x первое число
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Устанавливает новое значение второго числа
     * @param y второе число
     */
    public void setY(int y) {
        this.y = y;
    }  

    /**
     * Проверяет равенство данной пары и переданной.
     * @param obj пара, равенство с которой проверяется
     * @return равны ли пары чисел
     */
    @Override
    public boolean equals(Object obj) {
        if((obj instanceof Pair) && (((Pair)obj).getX() == x) && (((Pair)obj).getY() == y))
            return true;
        else return false;
    }

    /**
     * Хэшкод этой пары чисел
     * @return хашкод
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.x;
        hash = 67 * hash + this.y;
        return hash;
    }

    @Override
    public String toString() {
        return "(" + x + ";" + y + ")";
    }
    
    
    
    
}
