package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Map;
import com.kspt.group1.lambdaserver.simulator.Pair;
import java.util.ArrayList;

/**
 * Эвристика суммирующая расстояние до ближайшей лямбды + количество лямбд*25
 * @author Колтон Семен 5081/14
 */
public class ClosestPlusNumberHeuristic implements Heuristic{

    private int factor; // множитель перед количеством лямбд
    private Pair lift;
    
    public ClosestPlusNumberHeuristic(Map map) {
        Pair mapSize = map.getMapSize();
        this.factor = (mapSize.getX() + mapSize.getY())*2;
        this.lift = map.getLiftPosition();
    }

    /**
     * Эвристика суммирующая расстояние до ближайшей лямбды + количество лямбд
     * @param gamestate состояние игры
     * @return эвристику
     */
    @Override
    public int getHeuristic(GameState gamestate) {
        ArrayList<Pair> ll = gamestate.getMap().getLambdaList();
        int heuristic = closestLambdaDistance(ll, gamestate.getMap().getRobotPosition());
        if(heuristic == 0){
            heuristic = manhattanDistance(lift, gamestate.getMap().getRobotPosition());
        }
        return heuristic + ll.size()*factor;
    }
    
    private int closestLambdaDistance(ArrayList<Pair> lambdaList, Pair self){
        int distance;
        int closestDistance = Integer.MAX_VALUE;
        for (Pair pair : lambdaList) {
            distance = manhattanDistance(pair, self);
            if(distance < closestDistance){
                closestDistance = distance;
            }
        }
        return closestDistance;
    }
    
    private int manhattanDistance(Pair first, Pair second){
        return Math.abs(first.getX() - second.getX()) + Math.abs(first.getY() - second.getY());
    }
    
    
}
