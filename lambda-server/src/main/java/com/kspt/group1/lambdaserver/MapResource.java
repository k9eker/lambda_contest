package com.kspt.group1.lambdaserver;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.restlet.data.Reference;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author Garik
 */
public class MapResource extends ServerResource {

    /**
     * Метод целиком читатет файл и записывает в строку
     */
    public static String readFile(String name) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while ((str = br.readLine()) != null) {
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }

    /**
     * Метод возращает список карт Список состоит из имен карт разделенных
     * пробелами
     */
    public static String GetListOfFiles() {
        StringBuilder sb = new StringBuilder();
        String[] list = null;
        File directory_maps;
        directory_maps = new File("src/main/resources/maps");
        if (directory_maps.isDirectory()) {
            list = directory_maps.list();
            for (int i = 0; i < list.length; i++) {
                sb.append(list[i]);
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    /**
     * @return карту в виде String или список карт в зависимости от запроса(см ниже)
     * если карты на сервере нет то будет отправлено сообщение "File not found!"
     * @throws IOException
     * @throws ResourceException
     * @throws Exception 
     * //Для того чтобы получить карту testmap6.txt нужно
     * ввести в браузер: http://localhost:8182/maps/testmap6.txt 
     * //Для того чтобы получить список карт- нужно ввести в браузер 
     * http://localhost:8182/maps
     */
    @Get
    public String retrieve() throws IOException, ResourceException, Exception {
        String map = null;
        //Get the name of map from reference
        String query = getReference().getRemainingPart();
        if (!query.equals("") && !query.equals("/")) {
            try {
                //get param from request
                map = readFile("src/main/resources/maps" + query);
                //call DAO with parameter            
            } catch (Exception e) {
                return "File not found!";
            }

            return map;
        } else {
            return GetListOfFiles();
        }

    }
}
