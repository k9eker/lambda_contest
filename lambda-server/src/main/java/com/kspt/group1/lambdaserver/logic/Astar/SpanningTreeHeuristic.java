package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Pair;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Эвристика, строящая минимальное остовное дерево по лямбдам, роботу и лифту
 * по алгоритму Прима
 * @author Колтон Семен 5081/14
 */
public class SpanningTreeHeuristic implements Heuristic{
    
    public SpanningTreeHeuristic() {
    }
  
    @Override
    public int getHeuristic(GameState gamestate) {
        ArrayList<Pair> lambdaList = gamestate.getMap().getLambdaList();
        Pair robotPosition = gamestate.getMap().getRobotPosition();
        Pair liftPosition = gamestate.getMap().getLiftPosition();
        
        // вес всего дерева
        int treeWeight = 0;
        
        PriorityQueue<Vertex> q = new PriorityQueue<Vertex>(lambdaList.size() + 2, new VertexComparator());
        // добавляем вершины
        Vertex root = new Vertex(robotPosition, null, 0);   // робот - это корень
        q.add(root);
        q.add(new Vertex(liftPosition, root));
        for (Pair lambda : lambdaList) {
            q.add(new Vertex(lambda, root));
        }
        
        while(!q.isEmpty()){
            Vertex u = q.remove();
            treeWeight += u.weight;
            for (Vertex vertex : q) {
                int dist = vertex.manhattanDistance(u);
                if(dist < vertex.weight){
                    vertex.parent = u;
                    vertex.weight = dist;
                }
            }
        }
        
        return treeWeight;
    }
    
    
    
    /**
     * Класс описывающий вершину дерева
     */
    private class Vertex{
        Pair u;
        Vertex parent;
        int weight;

        public Vertex(Pair u, Vertex parent, int weight) {
            this.u = u;
            this.parent = parent;
            this.weight = weight;
        }
        
        public Vertex(Pair u, Vertex parent) {
            this.u = u;
            this.parent = parent;
            this.weight = manhattanDistance(parent);
        }
        
        /**
        * Манхэттенское расстояние - весовая функция для остовного дерева
        */
        int manhattanDistance(Vertex v){
            return Math.abs(this.u.getX() - v.u.getX()) 
                    + Math.abs(this.u.getY() - v.u.getY());
        }
    }
    
    private class VertexComparator implements Comparator<Vertex>{

        public VertexComparator() {
        }

        public int compare(Vertex o1, Vertex o2) {
            if (o1.weight < o2.weight)
                return -1;
            else if (o1.weight == o2.weight)
                return 0;
            else
                return 1;
        }
    }
}
