package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.GameState;
import com.kspt.group1.lambdaserver.simulator.Simulator;
import java.util.ArrayList;

/**
 * Класс, описывающий одно состояние дерева поиска.
 * Содержит игровое состояние (карта + статистика), маршрут робота, который
 * привел в это состояние, оценку этого маршрута, а так же эвристику этого состояния
 * @author Колтон Семен 5081/14
 */
public class AstarState{
    // эвритика
    public static Heuristic heuristic;
    
    // игровое состояние
    private GameState gameState;
    // маршрут до данного состояния
    private String path;
    // стоимость этого маршрута
    private int g;
    // эвристика этого состояния
    private int h;

    /**
     * Конструктор. Эвристика для данного состояния определяется внутри
     */
    public AstarState(GameState gameState, String path, int g) {
        this.gameState = gameState;
        this.path = path;
        this.g = g;
        this.h = heuristic.getHeuristic(gameState);
    }

    public GameState getGameState() {
        return gameState;
    }

    public String getPath() {
        return path;
    }
    
    public int getScore(){
        return gameState.getGameStatistics().getPoints();
    }

    public int getG() {
        return g;
    }

    public int getH() {
        return h;
    }
    
    public int getF(){
        return g+h;
    }

    
    /**
     * Возвращает все возможные следующие состояния после данного.
     * @param sim симулятор
     * @return следующие состояния
     */
    public ArrayList<AstarState> getSuccessors(Simulator sim){
        ArrayList<AstarState> statesArray = new ArrayList<AstarState>();
        String[] moves = {"U", "D", "L", "R", "W"/*, "A"*/};
        if (gameState.getGameStatistics().isOnAir()){
            for (int i = 0; i < moves.length; i++) {
                sim.setGameState(gameState);
                switch(moves[i].charAt(0)){
                    case 'U':   sim.moveUp();
                                break;
                    case 'D':   sim.moveDown();
                                break;
                    case 'L':   sim.moveLeft();
                                break;
                    case 'R':   sim.moveRight();
                                break;
                    case 'W':   sim.moveWait();
                                break;
                    /*case 'A':   sim.moveAbort();
                                break;*/
                }
                GameState gs = sim.getGameState();
                if(gs.getGameStatistics().isLastMoveSuccessful() 
                        && gs.getGameStatistics().isRobotAlive()){
                    statesArray.add(new AstarState(gs, path.concat(moves[i]), g + StepCost.SimpleOneCost()));
                }
            }
        }
        
        return statesArray;  
    }

    @Override
    public int hashCode() {
        return h;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getGameState().getMap().equals(((AstarState)obj).getGameState().getMap());
    }
    
    
 
    
    
    
}
