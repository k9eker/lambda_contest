package com.kspt.group1.lambdaserver.logic.Astar;

import com.kspt.group1.lambdaserver.simulator.Pair;

/**
 * Проблема поиска ближайшей лямбды
 * @author Колтон Семен 5081/14
 */
public class ClosestLambdaProblem implements Problem{

    // координаты целевой лямбды
    Pair lambda;
    
    public ClosestLambdaProblem(int x, int y) {
        this.lambda = new Pair(x, y);
    }
    
    public ClosestLambdaProblem(Pair lambda){
        this.lambda = lambda;
    }

    @Override
    public boolean isGoalState(AstarState as) {
        return as.getGameState().getMap().getRobotPosition().equals(lambda);
    }
    
    
    
}
