package com.kspt.group1.lambdaserver.main;

import com.kspt.group1.lambdaserver.logic.Astar.AstarLogic;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * Класс для обработки сигналов
 */
public class InterruptSignalHandler implements SignalHandler {

    private SignalHandler oldHandler;
    private SignalHandler handler;
    
    private AstarLogic al;

    private InterruptSignalHandler() {
    }

    // Статический метод установки обработчика сигнала
    public static void install(String signalName, SignalHandler handler, AstarLogic al) {
        Signal signal = new Signal(signalName);
        InterruptSignalHandler signalHandler = new InterruptSignalHandler();
        SignalHandler oldHandler = Signal.handle(signal, signalHandler);
        signalHandler.setHandler(handler);
        signalHandler.setOldHandler(oldHandler);
        signalHandler.setLogic(al);
    }

    private void setLogic(AstarLogic al) {
        this.al = al;
    }

    private void setOldHandler(SignalHandler oldHandler) {
        this.oldHandler = oldHandler;
    }

    private void setHandler(SignalHandler handler) {
        this.handler = handler;
    }

    // Обработчик сигнала
    @Override
    public void handle(Signal sig) {
        try {
            Thread.sleep(8000);
        } catch (InterruptedException ex) {
            Logger.getLogger(InterruptSignalHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            handler.handle(sig);
            // Возврат к предыдущему обработчику, если он существует
            if (oldHandler != SIG_DFL && oldHandler != SIG_IGN) {
                oldHandler.handle(sig);
            }
        } catch (Exception e) {
            System.out.println(al.getCurrentSolve().concat("A"));
        }
    }
}