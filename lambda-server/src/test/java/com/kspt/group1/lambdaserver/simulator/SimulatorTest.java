package com.kspt.group1.lambdaserver.simulator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Тестирование симулятора.
 * @author Колтон Семен 5081/14
 */
public class SimulatorTest extends TestCase{
    private final String map;
    
    /**
     * Конструктор 
     */
    public SimulatorTest(String name) throws Exception{
        super(name);
        map = readFile("src/main/resources/maps/testmap3.txt");
    }

    
    public static Test suite() throws Exception{
        TestSuite suite = new TestSuite();
        suite.addTest(new SimulatorTest("testConstructor"));
        suite.addTest(new SimulatorTest("testRobotMove"));
        suite.addTest(new SimulatorTest("testMapResolve"));
        return suite;
    }
    
    /**
     * Тест конструктора
     */
    public void testConstructor(){
        Simulator sim = new Simulator(map);
        compareStringRepresentation(map, sim.getMapStringRepresentation());
        assertEquals(Cell.ROBOT, sim.getCell(sim.getRobotPosition()));
        assertEquals(Cell.ROBOT, sim.getMap().getCell(sim.getRobotPosition()));
        assertEquals(0, sim.getLambdaEaten());
        assertEquals(0, sim.getGameStatistics().getLambdaEaten());
        assertEquals(Cell.CLOSED_LIFT, sim.getCell(sim.getLiftPosition()));
        assertEquals(0, sim.getPoints());
        
        GameStatistics gs = new GameStatistics();
        assertEquals(gs, sim.getGameStatistics());
        assertEquals(gs, sim.getGameState().getGameStatistics());
        
        Map m = new Map(map);
        assertEquals(m, sim.getMap());
        assertEquals(m, sim.getGameState().getMap());
    }
 
    
    /**
     * Тест движения робота
     */
    public void testRobotMove(){
        Simulator sim = new Simulator(map);
        // произвели один шаг вправо
        GameStatistics gs = sim.moveRight().getGameStatistics();
        // проверяем статистику
        assertEquals(0, gs.getLambdaEaten());
        assertEquals(-1, gs.getPoints());
        assertEquals(1, gs.getSteps());
        assertEquals(true, gs.isLastMoveSuccessful());
        assertEquals(false, gs.isLiftOpened());
        assertEquals(true, gs.isOnAir());
        assertEquals(true, gs.isRobotAlive());
        // проверяем карту
        assertEquals(new Pair(5, 3), sim.getRobotPosition());
        assertEquals(Cell.ROBOT, sim.getCell(sim.getRobotPosition()));
        assertEquals(Cell.STONE, sim.getCell(3, 3));
        
        
        // завершаем игру абортом)))
        gs = sim.moveAbort().getGameStatistics();
        // проверяем статистику
        assertEquals(0, gs.getLambdaEaten());
        assertEquals(-1, gs.getPoints());
        assertEquals(1, gs.getSteps());
        assertEquals(true, gs.isLastMoveSuccessful());
        assertEquals(false, gs.isLiftOpened());
        assertEquals(false, gs.isOnAir());
        assertEquals(true, gs.isRobotAlive());
        // проверяем карту
        assertEquals(new Pair(5, 3), sim.getRobotPosition());
        assertEquals(Cell.ROBOT, sim.getCell(sim.getRobotPosition()));
        assertEquals(Cell.STONE, sim.getCell(3, 3));
    }
    
    
    /**
     * Тест прохода по карте
     */
    public void testMapResolve(){
        Simulator sim = new Simulator(map);
        GameStatistics gs;
        // решение карты
        String resolve = "RUURLURURRUULLDDDLDDDDLRWWWRRRRR";
        for (int i = 0; i < resolve.length(); i++) {
            char c = resolve.charAt(i);
            switch(c){
                case 'R':
                    sim.moveRight();
                    break;
                   
                case 'L':
                    sim.moveLeft();
                    break;
                    
                case 'U':
                    sim.moveUp();
                    break;
                    
                case 'D':
                    sim.moveDown();
                    break;
                    
                case 'W':
                    sim.moveWait();
                    break;
                    
                case 'A':
                    sim.moveAbort();
                    break;
            }
        }     
        gs = sim.getGameStatistics();
        assertEquals(7, gs.getLambdaEaten());
        assertEquals(32, gs.getSteps());
        assertEquals(493, gs.getPoints());
        assertEquals(true, gs.isLastMoveSuccessful());
        assertEquals(true, gs.isLiftOpened());
        assertEquals(false, gs.isOnAir());
        assertEquals(true, gs.isRobotAlive());
        
        // проверяем карту
        assertEquals(new Pair(10, 2), sim.getRobotPosition());
        assertEquals(Cell.ROBOT, sim.getCell(sim.getRobotPosition()));
        assertEquals(Cell.FREE, sim.getCell(3, 3)); 
        assertEquals(Cell.FREE, sim.getCell(9, 9)); 
        assertEquals(Cell.FREE, sim.getCell(4, 5)); 
        assertEquals(Cell.GROUND, sim.getCell(3, 5)); 
        assertEquals(Cell.STONE, sim.getCell(6, 5)); 
        assertEquals(Cell.STONE, sim.getCell(7, 6));
        assertEquals(Cell.STONE, sim.getCell(4, 8)); 
    }

    /**
     * Метод сравнивает два строковых представления карты.
     */
    private void compareStringRepresentation(String map, String repr){
        String [] mapArr = map.split("\n");
        String [] reprArr = repr.split("\n");
       
        assertEquals(mapArr.length, reprArr.length);
        for (int i = 0; i < mapArr.length; i++) {
            mapArr[i] = mapArr[i].trim();
            reprArr[i] = reprArr[i].trim();
        }
        
        for (int i = 0; i < mapArr.length; i++) {
            assertEquals(mapArr[i], reprArr[i]);
        }
    }
    
     /**
     * Метод целиком читатет файл и записывает в строку
     */
    private String readFile(String name) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while((str = br.readLine()) != null){
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }
}
