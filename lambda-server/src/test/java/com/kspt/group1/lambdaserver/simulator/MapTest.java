package com.kspt.group1.lambdaserver.simulator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Тесты класса Map.
 * @author Колтон Семен 5081/14
 */
public class MapTest extends TestCase{

    private final String map1;
    private final String map2;
    private final String map3;
    private final String emptymap = "";
    private final String wrongmap = "####\n##g#\n#";
    private Map m;
    
    /**
     * Конструктор 
     */
    public MapTest(String name) throws Exception{
        super(name);
        map1 = readFile("src/main/resources/maps/testmap1.txt");
        map2 = readFile("src/main/resources/maps/testmap2.txt");
        map3 = readFile("src/main/resources/maps/testmap3.txt");
    }
        
    
    public static Test suite() throws Exception{
        TestSuite suite = new TestSuite();
        suite.addTest(new MapTest("testConstructor"));
        suite.addTest(new MapTest("testGetCell"));
        suite.addTest(new MapTest("testSetCell"));
        suite.addTest(new MapTest("testSetters"));
        suite.addTest(new MapTest("testCloneEquals"));
        return suite;
    }

    
    /**
     * Тест конструктора класса Map
     */
    public void testConstructor(){
        Pair robotPosition;
        Pair liftPosition;
        Pair mapSize;
        // проверка того, как парсится карта
        m = new Map(map1);
        compareStringRepresentation(map1, m.getMapStringRepresentation());
        mapSize = new Pair(30, 15);
        robotPosition = new Pair(7, 8);
        liftPosition = new Pair(7, 15);
        assertEquals(mapSize, m.getMapSize());
        assertEquals(robotPosition, m.getRobotPosition());
        assertEquals(liftPosition, m.getLiftPosition());
        
        m = new Map(map2);
        compareStringRepresentation(map2, m.getMapStringRepresentation());
        mapSize = new Pair(19, 17);
        robotPosition = new Pair(3, 15);
        liftPosition = new Pair(14, 1);
        assertEquals(mapSize, m.getMapSize());
        assertEquals(robotPosition, m.getRobotPosition());
        assertEquals(liftPosition, m.getLiftPosition());
        
        m = new Map(map3);
        compareStringRepresentation(map3, m.getMapStringRepresentation());
        ArrayList<Pair> lambdaList = new ArrayList<Pair>();
        lambdaList.add(new Pair(4, 2));
        lambdaList.add(new Pair(5, 2));
        lambdaList.add(new Pair(6, 5));
        lambdaList.add(new Pair(7, 7));
        lambdaList.add(new Pair(8, 7));
        lambdaList.add(new Pair(6, 9));
        lambdaList.add(new Pair(7, 9));
        assertLambdaListsEquals(lambdaList, m.getLambdaList());
        mapSize = new Pair(10, 10);
        robotPosition = new Pair(4, 3);
        liftPosition = new Pair(10, 2);
        assertEquals(mapSize, m.getMapSize());
        assertEquals(robotPosition, m.getRobotPosition());
        assertEquals(liftPosition, m.getLiftPosition());
        
        
        try{
            m = new Map(emptymap);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        try{
            m = new Map(wrongmap);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
    }
    
    /**
     * Тест метода getCell
     */
    public void testGetCell(){
        m = new Map(map3);
        // должно работать правильно
        assertEquals(Cell.WALL, m.getCell(1, 1));
        assertEquals(Cell.WALL, m.getCell(1, 10));
        assertEquals(Cell.WALL, m.getCell(10, 1));
        assertEquals(Cell.WALL, m.getCell(10, 10));
        assertEquals(Cell.ROBOT, m.getCell(4, 3));
        assertEquals(Cell.STONE, m.getCell(6, 7));
        // должно выдать исключение
        try{
            m.getCell(-1, 0);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        try{
            m.getCell(15, 15);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        // должно работать правильно
        assertEquals(Cell.WALL, m.getCell(new Pair(1, 1)));
        assertEquals(Cell.WALL, m.getCell(new Pair(1, 10)));
        assertEquals(Cell.ROBOT, m.getCell(new Pair(4, 3)));
        // должно бросить исключение
        try{
            m.getCell(new Pair(15, 15));
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
    }
    
    /**
     * тест метода setCell
     */
    public void testSetCell(){
        m = new Map(map3);
        m.setCell(2, 2, Cell.WALL);
        m.setCell(3, 3, Cell.WALL);
        m.setCell(new Pair(4, 4), Cell.WALL);
        m.setCell(new Pair(5, 5), Cell.WALL);
        try{
            m.setCell(-1, 0, Cell.WALL);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        try{
            m.setCell(new Pair(15, 15), Cell.WALL);
            fail("test must fail");
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        assertEquals(Cell.WALL, m.getCell(2, 2));
        assertEquals(Cell.WALL, m.getCell(3, 3));
        assertEquals(Cell.WALL, m.getCell(new Pair(4, 4)));
        assertEquals(Cell.WALL, m.getCell(new Pair(5, 5)));    
    }
    
    /**
     * тесты сеттеров
     */
    public void testSetters(){
        m = new Map(map3);
        ArrayList<Pair> lambdaList = new ArrayList<Pair>();
        lambdaList.add(new Pair(4, 2));
        lambdaList.add(new Pair(5, 2));
        lambdaList.add(new Pair(6, 5));
        lambdaList.add(new Pair(7, 7));
        lambdaList.add(new Pair(8, 7));
        m.setLambdaList(lambdaList);
        assertLambdaListsEquals(lambdaList, m.getLambdaList());
        
        m.setRobotPosition(new Pair(1,1));
        assertEquals(new Pair(1,1), m.getRobotPosition());
    }
    
    
    public void testCloneEquals(){
        m = new Map(map3);
        Map clonedMap = (Map)m.clone();
        assertEquals(clonedMap, m);
    }
    
    
    /**
     * Метод сравнивает два строковых представления карты.
     */
    private void compareStringRepresentation(String map, String repr){
        String [] mapArr = map.split("\n");
        String [] reprArr = repr.split("\n");
       
        assertEquals(mapArr.length, reprArr.length);
        for (int i = 0; i < mapArr.length; i++) {
            mapArr[i] = mapArr[i].trim();
            reprArr[i] = reprArr[i].trim();
        }
        
        for (int i = 0; i < mapArr.length; i++) {
            assertEquals(mapArr[i], reprArr[i]);
        }
    }
    
    
    /**
     * Метод целиком читатет файл и записывает в строку
     */
    private String readFile(String name) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(name));
        String result = new String();
        String str = new String();
        while((str = br.readLine()) != null){
            result = result.concat(str + '\n');
        }
        br.close();
        return result;
    }
    
    /**
     * проверка того, что два списка лямбд совпадают 
     */
    private void assertLambdaListsEquals(ArrayList<Pair> expected, ArrayList<Pair> real){
        assertTrue(expected.containsAll(real));
        assertTrue(real.containsAll(expected));
    }
    
}
