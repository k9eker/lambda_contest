/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kspt.group1.lambdaserver.simulator;

import junit.framework.TestCase;

/**
 * Тестовый класс для тестирования класса Cell
 * @author Колтон Семен 5081/14
 */
public class CellTest extends TestCase{

    public CellTest(String name) {
        super(name);
    }
    
    /**
     * Тест метода valueOf(char)
     */
    public void testValueOf(){
        assertEquals(Cell.CLOSED_LIFT, Cell.valueOf('L'));
        assertEquals(Cell.OPENED_LIFT, Cell.valueOf('O'));
        assertEquals(Cell.FREE, Cell.valueOf(' '));
        assertEquals(Cell.GROUND, Cell.valueOf('.'));
        assertEquals(Cell.LAMBDA, Cell.valueOf('\\'));
        assertEquals(Cell.ROBOT, Cell.valueOf('R'));
        assertEquals(Cell.STONE, Cell.valueOf('*'));
        assertEquals(Cell.WALL, Cell.valueOf('#'));
        assertNull(Cell.valueOf('a'));
        assertNull(Cell.valueOf('%'));
    }
    
    
    /**
     * Тест метода toChar()
     */
    public void testToChar(){
        assertEquals('L', Cell.CLOSED_LIFT.toChar());
        assertEquals('O', Cell.OPENED_LIFT.toChar());
        assertEquals(' ', Cell.FREE.toChar());
        assertEquals('.', Cell.GROUND.toChar());
        assertEquals('\\', Cell.LAMBDA.toChar());
        assertEquals('R', Cell.ROBOT.toChar());
        assertEquals('*', Cell.STONE.toChar());
        assertEquals('#', Cell.WALL.toChar());
    }
    
}
